#include "Products.h"
#include <iostream>
#include <fstream>
#include <deque>
#include <algorithm>

int main()
{
	std::deque<Product> products;
	uint16_t m_id;
	std::string m_name;
	float m_pret;
	uint16_t m_vat;
	std::string m_dateOrType;
	for (std::ifstream inputfile("products.prodb"); !inputfile.eof();)
	{
		inputfile >> m_id >> m_name >> m_pret >> m_vat >> m_dateOrType;
		//validate input...
		products.emplace_back(m_id, m_name, m_pret, m_vat, m_dateOrType);
	}
	system("pause");
	return 0;
}